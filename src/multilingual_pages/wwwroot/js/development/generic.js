
/** generic constants */
export const actions = {
    LIST_LOADING: "_LIST_LOADING",
    CURRENT_LOADING: "_CURRENT_LOADING",
    CURRENT_SAVING: "_CURRENT_SAVING",

    ITEM_DELETING: "_ITEM_DELETING",
    ITEM_DELETED: "_ITEM_DELETED",

    SET_LIST:  "_SET_LIST",
    SET_CURRENT: "_SET_CURRENT",

    UPDATE_CURRENT_PROPERTY: "_UPDATE_CURRENT_PROPERTY",

    LIST_LOADING_ERROR: "_LIST_LOADING_ERROR",
    CURRENT_LOADING_ERROR: "_CURRENT_LOADING_ERROR",
    CURRENT_SAVING_ERROR: "_CURRENT_SAVING_ERROR",
    ITEM_DELETING_ERROR: "_ITEM_DELETING_ERROR",

    RESET: "_RESET"
}

export const LIST_STATE_PROPERTY = "list";
export const CURRENT_STATE_PROPERTY = "current";

/** generic reducers */
export const reducer_withList = (actionPrefix, state, action, listName) => {
    listName = listName || LIST_STATE_PROPERTY;
    const enhanced =  {
                    [actionPrefix + actions.LIST_LOADING]: () => {return {...state, [listName + "_loading"]: true, [listName + "_loading_error"]: ""}},

                    [actionPrefix + actions.SET_LIST]: () => {                      
                        return {...state, [listName]: action.payload, [listName + "_loading"]:false}
                    },
                    [actionPrefix + actions.LIST_LOADING_ERROR]: () => {return {...state,  [listName + "_loading_error"]: action.payload}}
    }

    // specific reducer overload geneic.
    return enhanced;
}


export const reducer_withItemsRemovingFromListByIndex = (actionPrefix, state, action, listName) => {
    listName = listName || LIST_STATE_PROPERTY;

    const enhanced = { 
     
        [actionPrefix + actions.ITEM_DELETED]: () => {
            const indexes = action.payload || [];

            let resultArray = state[listName] && state[listName].length > 0? state[listName].slice(0): [];

            for (let i = 0; i < indexes.length; i++) {
               
                const index = indexes[i];
                resultArray = resultArray.filter((item, elIndex) => elIndex !== index);
            }

            return { ...state, [listName]: resultArray, [listName + '_deleting']: false }
        }
    }
 
    return enhanced;   
 }

export const reducerWithReset = actionPrefix => ({ [actionPrefix + actions.RESET]: () => ({}) });


export const reducer_withItemsRemoving = (actionPrefix, state, action, statePropertyName) => {

    const enhanced = { 
            [actionPrefix + actions.ITEM_DELETING]: () => { 
                return {...state, [statePropertyName + "_deleting"]: true, [statePropertyName + "_deleting_error"]: ""}
            },
            [actionPrefix + actions.ITEM_DELETING_ERROR]: () => {
                return {...state, [statePropertyName + "_deleting_error"]: action.payload, [statePropertyName + "_deleting"]: false}
            }
    }

    return enhanced;   
}

export const reducer_withCurrent = (actionPrefix, state, action, currentName) => {
    currentName = currentName || CURRENT_STATE_PROPERTY;
   
    const enhanced = { 
        [actionPrefix + actions.CURRENT_LOADING]: () => {return {...state, [currentName + "_loading"]: true, [currentName + "_loading_error"]: ""}},
        [actionPrefix + actions.SET_CURRENT]: () => {return {...state, [currentName]: action.payload, [currentName + "_loading"]:false, [currentName + "_saving"]:false}},
        [actionPrefix + actions.CURRENT_SAVING]: () => {return {...state, [currentName + "_saving"]: true, [currentName + "_saving_error"]: "", [currentName + "_was_changes"]: false}},
    
        [actionPrefix + actions.CURRENT_LOADING_ERROR]: () => {return {...state, [currentName + "_loading_error"]: action.payload, [currentName + "_loading"]: false}},
        [actionPrefix + actions.CURRENT_SAVING_ERROR]: () => {return {...state, [currentName + "_saving_error"]: action.payload, [currentName + "_saving"]: false, [currentName + "_was_changes"]: false}},
      
        [actionPrefix + actions.UPDATE_CURRENT_PROPERTY]: () => {
            const {property, value} = action.payload;

            if (property) 
                return { ...state, [currentName]: {...state[currentName], [property]: value}, [currentName + "_was_changes"]: true  }
            else 
                return state;
        }
    }

     return enhanced;   
}

export const reducer_withPagination = (listName, state, action) => {
    listName = listName || LIST_STATE_PROPERTY;

    const enhanced = { 
       [name + actions.PAGE_CHANGED]: () => {return {...state, [listName + "_page"]: action.palyload}}
    }
     return enhanced;   
}


/* reducer helper */
export const getReducerResult = (currentReducer, type, state) => {
    let result = state;
    if (!currentReducer || !type) return result;

    const func = currentReducer[type];
    
    if (typeof func === 'function') result = func();
    return result;
}


/** generic SELECTORS */
export const selector_getList = (state) => state.list? state.list : [];
export const selector_getListLoading = state=> { 
    return  state && typeof state.list_loading !== 'undefined'? state.list_loading: true;
}
export const selector_getListLodingError = state=> state.list_loading_error || "";

export const selector_getItem = (state) => {
    return state && state.current? state.current: {};
} 
export const selector_getItemLoading = state=>  state && typeof state.current_loading !== 'undefined'? state.current_loading: false;
export const selector_getItemLodingError = state=> state.current_loading_error || "";
export const selector_getItemSaving = state=> state && typeof state.current_saving !== 'undefined'? state.current_saving: false;
export const selector_getSavingError = state=> state.current_saving_error || "";

export const selector_getDeletingFromList = state=> state && state.list_deleting != 'undefined'? state.list_deleting: false ;



/** generic operations */

export function apiHandler(args, dispatch) {
    //requered args
    let {url, method="get", dataToSent = null, errorHandlerType, pendingType, mockData} = args;
    //optional args
    let {errorCallback} = args;

    let apiFunc = method === "get"? httpGet.bind(this, url, dataToSent): httpPost.bind(this, url, method, dataToSent);

    if (!url) {
        console.log("Error in apiHandler, url is not defined");
        return;
    }

    if(pendingType)
        dispatch({type: pendingType});

    return apiFunc()
        .then( response => 
        {                
            // return response;
            return mockData; 

        })
        .catch(error => {
            console.log("%c actions error", "color: green");
            console.log(error);
                    
            if (typeof errorCallback === 'function')
                errorCallback(error);
    
            if (typeof error !== 'object'){  
                if (errorHandlerType)        
                    dispatch({type: errorHandlerType, payload: error});
            }
    
        })
}


function httpGet(url, urlArgs){
    return new Promise(function (resolve, reject) {

        setTimeout(function(){ 
            resolve([]);
         }, 500);       
    });
}


function httpPost(url, method, dataToSent){
    return new Promise(function (resolve, reject) {

        setTimeout(function(){ 
            resolve([]);
         }, 500);       
    });
}

export const updateCurrentItemField = (property, value, actionPrefix) => {
    return {
        type: [actionPrefix + actions.UPDATE_CURRENT_PROPERTY],
        payload: {property: property, value: value}      
    }
}

const cloneArray = (array) => {
    if (array instanceof Array)
        return array.slice(0); 

    console.log("Error when trying to clone array");
    return [];
}

export const addItemToList = (item, list) => {
    let listClone = cloneArray(list);
    let resultArray = listClone.concat(item);
    return resultArray;
}
