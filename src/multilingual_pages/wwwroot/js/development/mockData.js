/* MOCK DATA */
export const postsMockData = [
    {id: 1, date: "01.10.2018", author: "Vasya", name: "post 1 ",
        postContents: [
            {   id: 1, 
                postId: "1", 
                post:  {id: 1, date: "01.10.2018", author: "Vasya", name: "post 1 "},
                languageId: "2",
                language:  {id: 2, name: "en"},
                title: "post 1. on english",
                shortText:"bla bla", 
                longText: "bla bla bla and bla"
            },
            {   id: 2, 
                postId: "1", 
                post:  {id: 1, date: "01.10.2018", author: "Vasya", name: "post 1 "},
                languageId: "1", 
                language: {id: 1, name: "ru"},
                title: "запись 1. на русском", 
                shortText: "бла бла", 
                longText: "бла бла бла и бла" 
            },

        ]
    },
    {id: 2, date: "02.10.2018", author: "Tanya", name: "post 2",
        postContents: [ {   id: 3, 
            postid: "2", 
            post:  {id: 2, date: "02.10.2018", author: "Tanya", name: "post 2" },
            languageId: "1", 
            language:  {id: 1, name: "ru"},
            title: "запись 2. на русском", 
            shortText: "коротко о теме ", 
            longText: "полный текст" 
        }]
    },
    {id: 3, date: "03.10.2018", author: "Petya", name: "post 3"},
]

export const postsMultilingualMockData = [
    {   id: 1, 
        postId: "1", 
        post:  {id: 1, date: "01.10.2018", author: "Vasya", name: "post 1 "},
        languageId: "2",
        language:  {id: 2, name: "en"},
        title: "post 1. on english",
        shortText:"bla bla", 
        longText: "bla bla bla and bla"
    },
    {   id: 2, 
        postId: "1", 
        post:  {id: 1, date: "01.10.2018", author: "Vasya", name: "post 1 "},
        languageId: "1", 
        language: {id: 1, name: "ru"},
        title: "запись 1. на русском", 
        shortText: "бла бла", 
        longText: "бла бла бла и бла" 
    },
    {   id: 3, 
        postId: "2", 
        post:  {id: 2, date: "02.10.2018", author: "Tanya", name: "post 2" },
        languageId: "1", 
        language:  {id: 1, name: "ru"},
        title: "запись 2. на русском", 
        shortText: "коротко о теме ", 
        longText: "полный текст" 
    },
]

export const languagesMockData = [
    {id: 1, name: "ru"},
    {id: 2, name: "en" },
]


export const getMockPostMultilingual = (postId, languageId) => {

    let current = postsMultilingualMockData.find( (element) => {
        return element.postId == postId && element.languageId == languageId;
    });
    current = current? current: {};
    return current;
}

