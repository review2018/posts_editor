﻿import { combineReducers } from 'redux'
import {posts, post} from './features/post/postHandlers'
import {languages} from 'languages/languageHandlers'
import {post_content} from 'postContent/postContentHandlers'


export default combineReducers({
    posts: posts,
    post_content: post_content,
    post: post,
    languages: languages
})



