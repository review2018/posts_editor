import { apiHandler, actions, getReducerResult, updateCurrentItemField } from 'generic';
import { reducer_withCurrent } from 'generic';
import { getMockPostMultilingual } from 'mockData';


const POST_CONTENT_PREFIX = 'POST_CONTENT';

export function post_content(state = {}, action) {

    const postContentReducer = Object.assign({},
        reducer_withCurrent(POST_CONTENT_PREFIX, state, action),
    );
    return getReducerResult(postContentReducer, action.type, state);
}

/** SELECTORS */
export const selector_getPostContentObject = (postContent) => {
    if (!postContent) return null;

    const { id, title, shortText, longText, languageId, language = { name: '' }, post = { name: '' } } = postContent;

    return {
            id,
            title,
            shortText,
            longText,
            languageId,
            languageName: language.name,
            postName: post.name,
        };
};


export const selector_post_content = state => (state && state.post_content ? state.post_content: {});

/** ACTIONS */
function setPostContentAction(data) {
    return {
        type: POST_CONTENT_PREFIX + actions.SET_CURRENT,
        payload: data,
    };

}

/** OPERATIONS */
export function getPostContent(postId, languageId) {
    return dispatch => apiHandler({
                url: 'api/posts/getOne',
                dataToSent: { postId },
                pendingType: POST_CONTENT_PREFIX + actions.CURRENT_LOADING,
                errorHandlerType: POST_CONTENT_PREFIX + actions.CURRENT_LOADING_ERROR,
                mockData: getMockPostMultilingual(postId, languageId),
            },
            dispatch)
            .then((data = {postId, languageId}) => {
                dispatch(setPostContentAction(data));
                return (data);
            });
}

export function updatePostContentProperties(property, value)
{
    return updateCurrentItemField(property, value, POST_CONTENT_PREFIX);
}

