import {languagesMockData} from 'mockData'
import {updateCurrentItemField, apiHandler, actions, getReducerResult, LIST_STATE_PROPERTY} from 'generic'
import {reducer_withList, reducer_withItemsRemoving, reducer_withItemsRemovingFromListByIndex, reducer_withCurrent} from 'generic'




const LANGUAGES_PREFIX = 'LANGUAGES';

export function languages(state = {}, action) {

    const languagesReducer = Object.assign({}, 
        reducer_withList(LANGUAGES_PREFIX, state, action)
    ); 

    return getReducerResult(languagesReducer, action.type, state);
   
}

/* SELECTORS */
export const selector_languages = state => state && state.languages? state.languages: {};

/** ACTION CREATORS */

function setLanguagesAction (data){
    
    return {
        type: LANGUAGES_PREFIX + actions.SET_LIST,
        payload: data
    }
}

/*
const normalizeLanguage = (language) => {
    if (language){
        let current = {};
        current.id = language.id;
        current.name = language.name;
      
        return current;
    }
    return {};
}
*/

/** OPERATIONS */
export function getLanguages() {
    return (dispatch, getState) => {
      
           return apiHandler({
                url: "api/languages/getAll",
                pendingType: LANGUAGES_PREFIX + actions.LIST_LOADING,
                errorHandlerType: LANGUAGES_PREFIX + actions.LIST_LOADING_ERROR,  
                mockData : languagesMockData 
            },
            dispatch)
            .then(data => {
                dispatch(setLanguagesAction(data));

                return(data);
            });

    }
}



