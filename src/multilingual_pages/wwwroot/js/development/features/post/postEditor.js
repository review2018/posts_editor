
import { ErrorMessage } from 'ui/errorMessage'
import { SubmitButton } from 'ui/buttons'
import { InputText } from 'ui/inputs'
import { FormView, WithRequeredFieldsValidatation } from 'ui/form'

const PostForm = WithRequeredFieldsValidatation(FormView);

const PostEditorForm = (props) => { 

        const {post = {}, saving = false, updateProperty = () => {}, handleFormSubmit = () => {} } = props;

        return (
          <PostForm handleFormSubmit={handleFormSubmit}>
            <InputText 
              type={'text'}
              autoFocus
              required={true}
              handleChange={updateProperty.bind(this, "name")}
              value={post.name}
              label={'Name'}
              name={'Name'}
              htmlFor="Name"
            />
            <div style={{textAlign: 'right'}}>
              <SubmitButton {...props} onClickHandler={null} pending={saving} text='Add' />
            </div>
          </PostForm>
    )
}


export default PostEditorForm;
