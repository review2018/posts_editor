import {bindActionCreators } from 'redux'
import {connect } from 'react-redux'
import {ErrorMessage } from 'ui/errorMessage'
import {IconPreloader} from 'ui/icon'
import {selector_getList, selector_getListLoading, selector_getListLodingError, selector_getDeletingFromList, selector_getItem, selector_getItemSaving } from 'generic'
import {getPosts, selector_posts, selector_post, selector_getPostObject, deletePosts , addPost, addPostToList, updatePostProperties, resetPost } from './postHandlers'
import {DeleteButton, EditButton, AddButton} from 'ui/buttons'
import {Navigating } from 'helpers/navigation'
import PostEditorForm from 'posts/postEditor'

class PostsListPage extends React.Component {

    componentDidMount() {
        let {posts } = this.props;

        if (posts.length <= 0) {
            this.props.getPosts().then(state => {
                // console.log('%c updated state after async action:', "color: tomato");
                // console.log(this.props.state);
               });       
        }      
    }  

    navigateToPostEditorPage = (postId) => {
        Navigating.redirect("postContentEditor", {postId: postId, languageId: 1})
    }

    addPost = () => {
        const {addPostToList, addPost, posts, post, resetPost} = this.props;
        addPost(post, posts).then((newPost) => {
            addPostToList(newPost, posts).then(() => {
                console.log("");
                console.log("after all");
                resetPost();
            }) ;
        });
    }
   
    render() {      
    
        console.log("%c state", "color: tomato");
        console.log(this.props.state);
        console.log("");
    
        if (this.props.loading === true)
        {
            return <IconPreloader />  
        }
        
            return  (<PostsListPageView 
                        {...this.props} 
                        navigateToPostEditorPage={this.navigateToPostEditorPage}
                        selector_getPostObject = {selector_getPostObject} 
                        addPost = {this.addPost}
                    />)      
        }
}


const PostsListPageView = (props) => {
    let {loadingError ="", deletingError="", postSaving, addPost, updateProperty} = props;

    return (
      <div className='animated fadeIn postsList'>
        <div>      
            <ErrorMessage {...props} errorMessage={loadingError} />
            <ErrorMessage {...props} errorMessage={deletingError} />

                <h3 style={{textAlign:'center', marginTop:'4rem', marginBottom:'4rem'}}>Posts editor</h3>
                <div style={{marginBottom:'1em'}}>

                    <PostEditorForm                   
                        {...props}
                        saving = {postSaving}
                        updateProperty = {updateProperty}
                        handleFormSubmit = {addPost}
                     />

                </div>
            <PostListElements {...props} />
        </div>
      </div>
        )
};




class PostListElements extends React.Component {

    render (){
        let {posts = [], selector_getPostObject} = this.props;

        var postsList = posts.map((value, index) => {
            value = selector_getPostObject(value);
 
            return <PostListElement 
                        {...this.props}
                        key={value.id} 
                        index={index}
                        Id={value.id} 
                        Name={value.name} 
                        Date = {value.date}    
                        Author = {value.author}
                    />
        });

        return ( <div>
                    {posts.length == 0 ? (
                        <div>No elements</div>                               
                    ) 
                    :              
                    (<table className="table .table-striped">
                        <thead>
                            <tr>
                                <th scope="col">Id</th>
                                <th scope="col">Date</th>
                                <th scope="col">Name</th>
                                <th scope="col">Author</th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>              
                            {postsList}
                        </tbody>
                        </table>)
                
                    }
                </div>)
    }
}

const PostListElement = (props) => { 

        let {deleting = false, Id = "", Date = "", Name = "", Author = "", index} = props;
        let {navigateToPostEditorPage = null, deletePosts} = props;

        let postsIds = [];
        postsIds.push(index);

        return (
            <tr>
                <td scope="row">{Id}</td>
                <td>{Date}</td>
                <td>{Name}</td>
                <td>{Author}</td>
                <td style={{textAlign: 'right'}}><EditButton onClickHandler={navigateToPostEditorPage.bind(this, Id)} /></td>
                <td style={{textAlign: 'right'}}>
                    <DeleteButton pending={deleting} onClickHandler={deletePosts.bind(this, postsIds)} />
                </td>
            </tr>
        )
}


function mapStateToProps(state, ownProps) {
    let postsState = selector_posts(state);
    postsState = postsState? postsState: {};
    let postState = selector_post(state);

    return {
        state: state,
        post: (selector_getItem(postState)), 
        posts: selector_getList(postsState), 
        loading: selector_getListLoading(postsState), 
        loadingError: selector_getListLodingError(postsState),
        deleting: selector_getDeletingFromList(postsState),
        postSaving: selector_getItemSaving(postState),
        
    }
}
    
function mapDispatchToProps(dispatch) {
    console.log("mapDispathc");
    return {
        getPosts: () => dispatch(getPosts()),
        deletePosts: (postsIds) => dispatch(deletePosts(postsIds)),
        addPost: (post, posts) => dispatch(addPost(post, posts)),
        addPostToList: (post, list) => dispatch(addPostToList(post, list)),       
        updateProperty: (prop, value) => dispatch(updatePostProperties(prop, value)),
        resetPost: () => dispatch(resetPost())
               
    };
}
    
    
 export default connect(mapStateToProps, mapDispatchToProps)(PostsListPage)



