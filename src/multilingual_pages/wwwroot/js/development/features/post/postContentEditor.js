import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {ErrorMessage} from 'ui/errorMessage'
import {SubmitButton, Button, DeleteButton, EditButton} from 'ui/buttons'
import {InputText, TextArea} from 'ui/inputs'
import {FormView} from 'ui/form'
import {Icon} from 'ui/icon'
import {selector_getItem, selector_getItemLoading, selector_getItemLodingError, selector_getItemSaving, selector_getSavingError, selector_getDeletingFromList} from 'generic'
import {selector_getList} from 'generic'
import {getPost, selector_post, deletePosts, selector_getPostObject} from './postHandlers'
import {selector_languages, getLanguages} from 'languages/languageHandlers'
import {getPostContent, selector_getPostContentObject, selector_post_content, updatePostContentProperties} from 'postContent/postContentHandlers'
import {Navigating} from 'helpers/navigation'

class PostContentEditorPage extends React.Component {

   
    componentDidMount() {

        let {postId, getPostContent, getPost, getLanguages, languageId=null, languages=[]} = this.props;
        languageId = languageId? languageId: (languages && languages.length > 0? languages[0].id: null);

        if (postId){

            getPost(postId);
            // get post content because post dont have all info we need. 
            getPostContent(postId, languageId);

            getLanguages();
        }
    }

    handleFormSubmit = (e) => {
        Navigating.redirect("posts");
    }

    deletePost = (postId) => {
        this.props.deletePosts(postId).then(state => {
            Navigating.redirect("posts");
        });        
    }

    navigateToPostContent = (languageId) => {
        let {postId} = this.props;
        Navigating.redirect("postContentEditor", {postId: postId, languageId: languageId});
    }

    componentWillUnmount(){

    }

    render() {

        console.log("%c this.props.state", "color:green");
        console.log(this.props.state);
        console.log("");

        return (
            <PostContentEditorPageView 
                {...this.props} 
                deletePost={this.deletePost} 
                navigateToPostContent={this.navigateToPostContent} 
                handleFormSubmit={this.handleFormSubmit} 
            />
        )
    }
}


const PostContentEditorPageView = (props) => {
    let {loadingError ="", savingError="", currentPostContent ={}, languageId=null, post, deleting} = props;
    let {deletePost , navigateToPostContent} = props;

    return ( <div className={"postEditor animated fadeIn "}>  
                <h3 style={{textAlign: 'center'}}>Post editor</h3>
                <div>
                    <span>{post.name}</span>
                    <div className="btn-group" style={{marginLeft:'1em'}}>
                        <EditButton justIcon={true} pending={null} onClickHandler={null} />
                        <DeleteButton justIcon={true} pending={deleting} onClickHandler={deletePost.bind(this, [post.id-1])} />
                    </div>
                </div>
                <hr/>
                <h4>{currentPostContent && currentPostContent.id? "Update post": "Add post"}</h4>
                <div style={{marginBottom: '2em', textAlign: 'right'}}>
                    <PostLanguage {...props} currentLanguageId={languageId} onLanguageClick ={navigateToPostContent} />
                </div>

                <ErrorMessage {...props} errorMessage={loadingError} />
                <ErrorMessage {...props} errorMessage={savingError} />             

                <PostContentForm   
                    {...props}   
                    current = {currentPostContent}       
                />
            </div>)
};


const PostContentForm = (props) => { 

        let {current = {}, saving = false, updateProperty=()=>{}, handleFormSubmit=()=>{}} = props;

        return (<FormView>
                    <InputText type={'text'}
                               autoFocus
                               handleChange={updateProperty.bind(this, "title")}
                               value={current.title}
                               label= {'Title'}
                               name={'Title'}
                               htmlFor={'Title'} />
                    <TextArea 
                               value={current.shortText}
                               label={null}
                               disabled={false}
                               name={'ShortText'}
                               label={'Short text'}
                               rows = {2}
                               maxTextLength = {30 }
                               handleChange={updateProperty.bind(this, "title")} />
                    <TextArea 
                               value={current.longText}
                               label={null}
                               disabled={false}
                               name={'LongText'}
                               label = {'Long text'}
                               rows = {10}
                               maxTextLength = {3000}
                               handleChange={updateProperty.bind(this, "title")} />
               
                   <div style={{textAlign: 'right'}}>
                    <SubmitButton {...props} onClickHandler={handleFormSubmit} pending={saving}/>
                  </div>
                </FormView>
    )
}

const PostLanguage = (props) => {
 
    let {languages = null, languagesLoading=false, languagesLoadingError="", currentLanguageId=null, onLanguageClick = ()=>{}} = props;

    if(languagesLoading)
        return (null)

    languages  = languages || [];

    let items = languages.map((lang) => {

        return  <Button
                    key={lang.id} 
                    onClickHandler={onLanguageClick.bind(this, lang.id)}           
                    customClasses={currentLanguageId == lang.id? "btn btn-default disabled": "btn btn-default"} >
                    {lang.name}
                </Button>
    })

    if (items.length > 0)
        return <div>{items}</div>
    else 
        return (null)
}


function mapStateToProps(state, _ownProps) {
   
    let postContentState = selector_post_content(state);
    let postState = selector_post(state);
    postContentState = postContentState? postContentState : {};

    let languagesState = selector_languages(state);

    return {
        state: state,
        post: selector_getPostObject(selector_getItem(postState)),
        postLoading: selector_getItemLoading(postState), 
        postLoadingError: selector_getItemLodingError(postState),
        currentPostContent: selector_getPostContentObject(selector_getItem(postContentState)),
        loading: selector_getItemLoading(postContentState), 
        saving: selector_getItemSaving(postContentState),
        loadingError: selector_getItemLodingError(postContentState),
        savingError: selector_getSavingError(postContentState),
        deleting: selector_getDeletingFromList(postState),

        languages: selector_getList(languagesState), 
        languagesLoadingError: selector_getItemLodingError(languagesState),
        languagesLoadingError: selector_getItemLodingError(languagesState),
    }
}
    
function mapDispatchToProps(dispatch) {

    return {
        getPostContent: (id, languageId) => dispatch(getPostContent(id, languageId)),
        getPost:(postId) => dispatch(getPost(postId)),
        getLanguages: () => dispatch(getLanguages()),
        updateProperty: (prop, value) => dispatch(updatePostContentProperties(prop, value)),
        deletePosts: (postsIds) => dispatch(deletePosts(postsIds))
       /*
        handleFormSubmit: (data) => dispatch(saveLanguage(data)),
       */
    }
}

    
export default connect(mapStateToProps, mapDispatchToProps)(PostContentEditorPage)



