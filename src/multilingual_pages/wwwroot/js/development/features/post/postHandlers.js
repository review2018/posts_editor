import {apiHandler, actions, getReducerResult, LIST_STATE_PROPERTY, updateCurrentItemField} from 'generic'
import {reducer_withList, reducer_withItemsRemoving, reducer_withItemsRemovingFromListByIndex, reducer_withCurrent, addItemToList, reducerWithReset} from 'generic'
import {postsMockData } from 'mockData'
var dateFormat = require('dateformat');

/* CONSTANTS */
const POSTS_PREFIX = 'POSTS';
const POST_PREFIX = 'POST';

/* REDUCERS */
/**
 * 
 * state will looks like this 
 * {
    *  posts: {
    *   list: [],
    *   list_loading: false,
    *   list_loading_error: "",
    *   list_deleting: false,
    *   list_deleting_error: ""
    * }
    * post: {
    *   current: {}
    *   current_loading: false,
    *   current_loading_error: "",
    * }
    * 
    * post_content: {
    *   current: {}
    *   current_loading: false,
    *   current_loading_error: false
    * }
 * }
 */
export function posts(state = {}, action) 
{
    // example of custom action, overrides generic actions
    const postsReducerCustom =  {    
        'CUSTOM_ACTION': () => {return {...state}},
    }

    const postsReducer = Object.assign({}, 
        reducer_withList(POSTS_PREFIX, state, action),
        reducer_withItemsRemoving(POSTS_PREFIX, state, action, LIST_STATE_PROPERTY),
        reducer_withItemsRemovingFromListByIndex(POSTS_PREFIX, state, action),
        postsReducerCustom
    ); 

    return getReducerResult(postsReducer, action.type, state);
   
}

export function post(state = {}, action) {

    const postReducer = Object.assign({}, 
        reducer_withCurrent(POST_PREFIX, state, action),
        reducerWithReset(POST_PREFIX)
    ); 

    return getReducerResult(postReducer, action.type, state); 
}




/* SELECTORS */
export const selector_posts = state => state && state.posts? state.posts: {};
export const selector_post = state => state && state.post? state.post: {};


export const selector_getPostObject = post => {
    if (post){
        let currentPost = {};
        currentPost.id = post.id;
        currentPost.name = post.name;
        currentPost.date = post.date;
        currentPost.author = post.author;
        return currentPost;
    }
    return {};
}




/** ACTION CREATORS */

function setPostsAction (data){
    
    return {
        type: POSTS_PREFIX + actions.SET_LIST,
        payload: data
    }
}
function setPostAction (data){
    
    return {
        type: POST_PREFIX + actions.SET_CURRENT,
        payload: data
    }
}

function deletePostsAction(postsIds){
    return {
        type: POSTS_PREFIX + actions.ITEM_DELETED,
        payload: postsIds
    }
}

export const resetPost = () => ({
        type: POST_PREFIX + actions.RESET,
        payload: null,
});

/** OPERATIONS */
export function getPosts() {
    return (dispatch) => {
      
           return apiHandler({
                url: "api/posts/getAll",
                pendingType: POSTS_PREFIX + actions.LIST_LOADING,
                errorHandlerType: POSTS_PREFIX + actions.LIST_LOADING_ERROR,  
                mockData : postsMockData 
            },
            dispatch)
            .then(data => {
                dispatch(setPostsAction(data));
               
                //console.log("%c getPosts operation", "color: blue");
               // console.log(data);
                return(data);
            });

    }
}

export function getPost(postId) {
    return (dispatch) => {
      
           return apiHandler({
                url: "api/posts/get",
                dataToSent: "?id=" + postId,
                pendingType: POST_PREFIX + actions.CURRENT_LOADING,
                errorHandlerType: POST_PREFIX + actions.CURRENT_LOADING_ERROR,  
                mockData : postsMockData[postId-1]
            },
            dispatch)
            .then(data => {
                dispatch(setPostAction(data));

                return(data);
            });

    }
}
export const addPost = (post, posts) => {
    return (dispatch, getState) => {
 
        return apiHandler({
             url: "api/posts/add",
             dataToSent: post,
             pendingType: POST_PREFIX + actions.CURRENT_SAVING,
             errorHandlerType: POST_PREFIX + actions.CURRENT_SAVING_ERROR,  
             mockData : null
         },
         dispatch)
         .then(data => {
            // all this thing should happen on server
            let newPost = Object.assign({}, post);
            newPost.id = posts.length + 1;

            var now = new Date();
            newPost.date =  dateFormat(now, "dd.mm.yyyy");
            newPost.author = "Admin"

            dispatch(setPostAction(newPost));

             return(newPost);
         });

 }
}

export const addPostToList = (post, list) => {
    return (dispatch, state) => {
      
        const newList = addItemToList(post, list);
        dispatch(setPostsAction(newList));
        return Promise.resolve();
  }
}


export function deletePosts(postsIds) {

    return (dispatch) => {
      
           return apiHandler({
                url: "api/posts/deletePost",
                method: "post",
                dataToSent: {postsIds: postsIds},
                pendingType: POSTS_PREFIX + actions.ITEM_DELETING,
                errorHandlerType: POSTS_PREFIX + actions.ITEM_DELETING_ERROR,  
                mockData : null 
            },
            dispatch)
            .then(() => {
                    //alternativ: get new array without removed posts from server 
                    dispatch(deletePostsAction(postsIds));
                });

    }
}

export function updatePostProperties(property, value)
{
    return updateCurrentItemField(property, value, POST_PREFIX);
}


