import { Provider } from 'react-redux'
import { createStore, applyMiddleware , compose} from 'redux'
import combineReducers from './indexReducer'
import thunk from 'redux-thunk' 
import {stateNavigator} from './navigatorRoutes'
import {Layout} from './layout/layout'
import PostsListPage from 'posts/postsList'
import PostContentEditorPage from 'posts/postContentEditor'
import {ping} from './middelware/ping'


const store = configureStore(); 


function configureStore(initialState) {
    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
    const middlewares = [thunk, ping];
    const enhancer = composeEnhancers(
        applyMiddleware(...middlewares),
        // other store enhancers if any
      );   
    
    const store = createStore(combineReducers, initialState, enhancer) 
    return store;

}

stateNavigator.states.posts.navigated = function (data, asyncData) {

  ReactDOM.render(
       <Provider store={store}>
          <Layout>
            <PostsListPage />
          </Layout>
      </Provider>,
   document.getElementById('app'));
};


stateNavigator.states.postContentEditor.navigated = function (data, asyncData) {

  ReactDOM.unmountComponentAtNode(document.getElementById("app"));

  ReactDOM.render(
       <Provider store={store}>
          <Layout>
            <PostContentEditorPage postId={data.postId || null} languageId={data.languageId}/>
          </Layout>
      </Provider>,
   document.getElementById('app'));
};

// create navigation
stateNavigator.start();







