const routes = [
    { key: 'posts', route: '/' },  
    { key: 'postContentEditor', route: 'postContentEditor/postId{postId}/{languageId?}', defaultTypes: { postId: 'number', languageId: 'number'}, defaults: { languageId: 0 }},

];


export const stateNavigator = new Navigation.StateNavigator(routes);
