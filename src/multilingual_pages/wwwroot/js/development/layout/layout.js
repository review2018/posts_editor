

export const Layout = (props) =>{
    
    return (<div className='animated fadeIn '>      
                    <HeaderWithMenu />
                    <div style={{width: '60%', marginLeft:'20%'}} className="main">
                        {props.children} 
                    </div>
            </div>)
}


const HeaderWithMenu = (props) => {
    return (
        <div className="navbar">
            <div className="navbar-inner">       
                <PageMenu/>
            </div>
        </div>
    )
}

const PageMenu = (props) => {
    return (
        <ul className="nav mainMenu">
            <li className="active">
                <a href="#">Home</a>
            </li>
            <li><a href="#">Some page 1</a></li>
            <li><a href="#">Some page 2</a></li>
        </ul>
    )
}

