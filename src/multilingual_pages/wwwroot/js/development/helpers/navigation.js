import {stateNavigator} from 'navigatorRoutes'

// Usage: Navigating.Redirect(route, routeData)
export const Navigating =  function () {

    function redirect (route, routeData) {
        stateNavigator.navigate(route, routeData);
    }
    return {
        redirect: redirect
    }

}();

/* Usage:
<RouteLink 
    {...this.props}
    to={this.props.stateKey}
    navigationData={this.props.navigationData}
    className={this.props.className}
   >
</RouteLink>
*/

export const RouteLink = (props) => {


    let {to, navigationData, className = 'btn btn-default', stateNavigator}  = this.props;
    
    if (!to)
        console.warn("this.props.to in RouteLink is missing. Data for this route " + this.props);
    else if (!navigationData)
            console.warn("this.props.navigateionData in RouteLink is missing. Data for this route " + this.props);
    else if (!stateNavigator)
            console.warn("stateNavigator in RouteLink is missing. Data for this route " + this.props);


    return (<NavigationReact.NavigationLink 
                stateKey={to}
                navigationData={navigationData}
                className={className}
                stateNavigator={stateNavigator}>
                        {this.props.children}
            </NavigationReact.NavigationLink>)
};