 

export const WithRequeredFieldsValidatation = Component => class WithRequeredFieldsValidatation extends React.Component {

    validateRequiredAndHandleFormSubmit = (e) => {
        if(e) e.preventDefault();
        let requredFieldsIsProvided = e.target.checkValidity();
        if (requredFieldsIsProvided)
            this.props.handleFormSubmit();
        //else 
            // TODO: show validation error
    }

    render() {         
        return  (<Component {...this.props} handleFormSubmit={this.validateRequiredAndHandleFormSubmit} />)          
    }
}


export const FormView = (props) => {
    let {handleFormSubmit = null, handleFormChanged = null} = props;

    return (<form onChange={handleFormChanged} onSubmit={handleFormSubmit} formNoValidate>          
                {props.children}
            </form>)
};



