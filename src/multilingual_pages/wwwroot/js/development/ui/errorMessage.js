

export const ErrorMessage = (props) => {
    if (props.errorMessage)
        return  (<p className="serverMessage">{props.errorMessage}</p>)
    else 
        return(null)
}


export const SomeMessage = (props) => {
    if (props.message)
        return  (<p className="someMessage">{props.message}</p>)
    else
        return(null)
}
