import { IconPreloader, Icon } from 'ui/icon';

const ButtonBase = (props) => {
    const { pending = false, onClickHandler, customStyle = {}, customClasses = '' , buttonType = 'button'} = props;

    return (
      <button
        style={customStyle}
        type={buttonType}
        className={pending ? 'disabled btn ' + customClasses: 'btn ' + customClasses}
        onClick={onClickHandler}>
        {props.children}
      </button>
        );
};
export const Button = ButtonBase;

export const DeleteButton = (props) => {
    if (props.justIcon) {
        return (
          <ButtonBase {...props} customClasses="btn-default">
            <Icon icon="fa fa-trash" />
          </ButtonBase>
               );
    }
    return (<ButtonBase {...props} customClasses="btn-danger">Delete</ButtonBase>);
};


export const EditButton = (props) => {
    if (props.justIcon) {
        return (
          <ButtonBase {...props} customClasses="btn-default">
            <Icon icon="fa fa-pencil" />
          </ButtonBase>
        );
    }
    return (<ButtonBase { ...props } customClasses="btn-link">Edit</ButtonBase>);

}

export const AddButton = (props) => {
  return (<ButtonBase { ...props } customClasses="btn-default"><span style={{marginRight: '2px'}}><Icon icon={"fa fa-plus"} /></span>Add</ButtonBase>);

}


export const SubmitButton = (props) => {
    const { pending = false, customClasses = 'btn btn-info', onClickHandler = null, text = 'Submit' } = props;

    return (
      <ButtonBase {...props} customClasses={customClasses} buttonType={'submit'}>
        {!pending ? text : <span> <IconPreloader />Sending</span>}
      </ButtonBase>
);
};
