export class InputText extends React.Component {

    constructor(props) {
        super(props);

        // handle inputs change 
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e) {
        this.props.handleChange(e.target.value);
    }

   
    render() {
        let opts = {};
        if (this.props.ElementId) {
            opts['id'] = this.props.ElementId;
        }
        if (this.props.disabled === true)
        {
            opts['disabled'] = 'disabled';
        }
        if (this.props.placeholder) {
            opts['placeholder'] = this.props.placeholder;
        }
        if (this.props.readonly)
        {
            opts['readonly'] = this.props.readonly;
        }
        if (this.props.step)
        {
            opts['step'] = this.props.step
        }
        if (this.props.autoFocus)
        {
            opts["autoFocus"] = true;
        }
        if (this.props.min) {
            opts["min"] = this.props.min;
        }
        if (this.props.required)
            opts["required"] = true;

        let label;
        if (this.props.label)
        {
            label = (<label htmlFor={this.props.htmlFor }>{this.props.label}</label>);
        }

        return (
                <div className={this.props.parentClass || 'form-group'}>
                    {label}
                    <input
                        type={this.props.type || 'text'}
                        value={this.props.value || ''}
                        ref={this.props.name || ''}
                        rows={this.props.rows || ''}
                        name={this.props.name}
                        className={this.props.className || 'form-control'}
                        onChange={this.handleChange}
                        {...opts} 
                     />
                    {this.props.maxTextLength? <InputLength text={this.props.value} maxTextLength={this.props.maxTextLength} />: ''}
                </div>
        )
    }

}

export class TextArea extends React.Component {

    constructor(props) {
        super(props);

        // handle inputs change 
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e) {
        this.props.handleChange(e.target.value);
    }

    render() {
        let opts = {};
        if (this.props.ElementId) {
            opts['id'] = this.props.ElementId;
        }
        if (this.props.disabled === true) {
            opts['disabled'] = 'disabled';
        }
        if (this.props.autoFocus)
        {
            opts["autoFocus"] = true;
        }

        let label;
        if (this.props.label)
        {
            label = (<label htmlFor={this.props.htmlFor }>{this.props.label}</label>);
    }

    return (
            <div className={this.props.parentClass || 'form-group'}>
                {label}
                <textarea
                   
                    value={this.props.value || ''}
                    ref={this.props.name || ''}
                    rows={this.props.rows || '1'}
                    name={this.props.name}
                    className={this.props.className || 'form-control'}
                    onChange={this.handleChange}
                    readOnly = {this.props.readonly || false}
                    placeholder = {this.props.placeholder || ''}
                    {...opts} 
                 />
                
            </div>
    )
  }
}
