
export const Icon = (props) => {

    let iconStyle = {
        marginRight: "2px"
    }

    let {icon, style = {} } = props;

    iconStyle = {...iconStyle, style }

    return (     
         <i style={iconStyle} className= { icon } aria-hidden="true"></i>
    )
}

export const IconPreloader = (prop) => {

    return <Icon style={prop.style} icon="fa fa-spinner fa-spin fa-fw"></Icon>

}

