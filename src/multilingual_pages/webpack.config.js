﻿/// <binding ProjectOpened='Watch - Development' /> 
"use strict";

var path = require('path')
var webpack = require('webpack')
var WebpackNotifierPlugin = require('webpack-notifier');


module.exports = {
    entry: './wwwroot/js/development/router.js',

    resolve: {
        extensions: ['.js', '.jsx'],
        alias: {
            generic: path.resolve('./wwwroot/js/development/generic'),
            posts: path.resolve('./wwwroot/js/development/features/post'),
            postContent: path.resolve('./wwwroot/js/development/features/postContent'),
            languages: path.resolve('./wwwroot/js/development/features/language'),
            ui: path.resolve('./wwwroot/js/development/ui'),
            mockData: path.resolve('./wwwroot/js/development/mockData.js'),
            helpers: path.resolve('./wwwroot/js/development/helpers'),
            navigatorRoutes: path.resolve('./wwwroot/js/development/navigatorRoutes.js'),
        },
    },
    output: {
        filename: "./wwwroot/js/production/site.js"
    },
    devServer: {
        contentBase: ".",
        host: "localhost",
        port: 9000,
        stats: { chunks: false }
    },
    module: {
        loaders: [
            {
                test: /\.js?$/,
                loader: "babel-loader",              
            },
            {
                test: /\.jsx?$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            },
            { 
            test: /\.css$/, 
                    loader: "style-loader!css-loader" 
            },
            {
                test: /\.scss$/,
                loaders: ['style-loader', 'css-loader', 'sass-loader']
            }
           
        ]
    },
    plugins: [
        new WebpackNotifierPlugin({ alwaysNotify: true })
    ]
};